# Changelog

## Current
- [#4] - Fix leaderboard name missing

## 2.6.0

- [#3] - Automatic tagging CI/CD

## 2.5.0

- [#1549] - Add Pull Request template and add CHANGELOG.md
- [#1551] - Add Address Name Service

## 2.3.0

- [#1546] - Removed calculateWinnings calls and replace with query to backend
- [#1547] - Added leaderboard for return ratio
